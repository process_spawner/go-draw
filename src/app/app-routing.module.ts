import { LayeredDiagramComponent } from './components/layered-diagram/layered-diagram.component';
import { DiagramComponent } from './components/diagram/diagram.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '',   redirectTo: 'drawer', pathMatch: 'full' },
  { path: 'diagram', component: LayeredDiagramComponent},
  { path: '**', component: LayeredDiagramComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
