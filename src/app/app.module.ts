import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DiagramComponent } from './components/diagram/diagram.component';
import { LayeredDiagramComponent } from './components/layered-diagram/layered-diagram.component';

@NgModule({
  declarations: [
    AppComponent,
    DiagramComponent,
    LayeredDiagramComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
