import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FakeComponentsService {

  constructor() { }

  getModels(): Array<any> {
    return [
      {"key":1, "type":"Table", "name":"Product"},
      {"key":2, "type":"Table", "name":"Sales"},
      {"key":3, "type":"Table", "name":"Period"},
      {"key":4, "type":"Table", "name":"Store"},
      {"key":11, "type":"Join", "name":"Product, Class"},
      {"key":12, "type":"Join", "name":"Period"},
      {"key":13, "type":"Join", "name":"Store"},
      {"key":21, "type":"Project", "name":"Product, Class"},
      {"key":31, "type":"Filter", "name":"Boston, Jan2014"},
      {"key":32, "type":"Filter", "name":"Boston, 2014"},
      {"key":41, "type":"Group", "name":"Sales"},
      {"key":42, "type":"Group", "name":"Total Sales"},
      {"key":51, "type":"Join", "name":"Product Name"},
      {"key":61, "type":"Sort", "name":"Product Name"},
      {"key":71, "type":"Export", "name":"File"}
        ];
  }

  getLinks(): Array<any> {
    return [
      {"from":1, "frompid":"OUTISH-1", "to":11, "topid":"L", "colors": ['green', 'yellow']},
      {"from":2, "frompid":"OUTISH-1", "to":11, "topid":"R"},
      {"from":3, "frompid":"OUTISH-1", "to":12, "topid":"R"},
      {"from":4, "frompid":"OUTISH-1", "to":13, "topid":"R"},
      {"from":11, "frompid":"M", "to":12, "topid":"L"},
      {"from":12, "frompid":"M", "to":13, "topid":"L", "colors": ['red', 'blue']},
      {"from":13, "frompid":"M", "to":21},
      {"from":21, "frompid":"OUTISH-1", "to":31},
      {"from":21, "frompid":"OUTISH-1", "to":32},
      {"from":31, "frompid":"OUTISH-1", "to":41, "colors": ['white', 'pink']},
      {"from":32, "frompid":"OUTISH-1", "to":42},
      {"from":41, "frompid":"OUTISH-1", "to":51, "topid":"L"},
      {"from":42, "frompid":"OUTISH-1", "to":51, "topid":"R", "colors": ['yellow', 'blue']},
      {"from":51, "frompid":"OUTISH-1", "to":61},
      {"from":61, "frompid":"OUTISH-1", "to":71}
        ];
  }

}
