import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FakeService {

  constructor() { }

  getModels(): Array<any> {
    const leftColor = '#F97F51';
    const rightColor = '#B33771';

    return [
      {"key":1, "name":"Unit One nine nine five",
       "leftArray":[ {"portColor": leftColor, "portId":"left0"} ],
       "topArray":[ {"portColor":"#d6effc", "portId":"top0"} ],
       "bottomArray":[ {"portColor":"#ebe3fc", "portId":"bottom0"} ],
       "rightArray":[ {"portColor": rightColor, "portId":"right0"},{"portColor": rightColor, "portId":"right1"} ] },
      {"key":2, "name":"Unit Two",
       "leftArray":[ {"portColor":leftColor, "portId":"left0"},{"portColor":leftColor, "portId":"left1"},{"portColor": leftColor, "portId":"left2"} ],
       "topArray":[ {"portColor":"#d6effc", "portId":"top0"} ],
       "bottomArray":[ {"portColor":"#eaeef8", "portId":"bottom0"},{"portColor":"#eaeef8", "portId":"bottom1"},{"portColor":"#6cafdb", "portId":"bottom2"} ],
       "rightArray":[  ] },
      {"key":3, "name":"Unit Three",
       "leftArray":[ {"portColor": leftColor, "portId":"left0"},{"portColor": leftColor, "portId":"left1"},{"portColor": leftColor, "portId":"left2"} ],
       "topArray":[ {"portColor":"#66d6d1", "portId":"top0"} ],
       "bottomArray":[ {"portColor":"#6cafdb", "portId":"bottom0"} ],
       "rightArray":[  ] },
      {"key":4, "name":"Unit Four",
       "leftArray":[ {"portColor": leftColor, "portId":"left0"} ],
       "topArray":[ {"portColor":"#6cafdb", "portId":"top0"} ],
       "bottomArray":[ {"portColor":"#6cafdb", "portId":"bottom0"} ],
       "rightArray":[ {"portColor": rightColor, "portId":"right0"},{"portColor": rightColor, "portId":"right1"} ] }
    ];
  }

  getLinks(): Array<any> {
    return [
      {"from":4, "to":2, "fromPort":"top0", "toPort":"bottom0" ,"colors":[ "#44bd32","#8c7ae6" ]},
      {"from":4, "to":2, "fromPort":"right1", "toPort":"bottom2","colors":[ "#e84118" ]},
      {"from":3, "to":2, "fromPort":"top0", "toPort":"bottom1","colors":[ "#dcdde1" ]},
      {"from":4, "to":3, "fromPort":"right0", "toPort":"left0","colors":[ "#0097e6" ]},
      {"from":4, "to":3, "fromPort":"right1", "toPort":"left2","colors":[ "#fbc531" ]},
      {"from":1, "to":2, "fromPort":"right0", "toPort":"left1","colors":[ "#8c7ae6","#44bd32" ]},
      {"from":1, "to":2, "fromPort":"right1", "toPort":"left2","colors":[ "#192a56", ]}
    ];
  }
}
