import { MultiColorLink } from './../components/layered-diagram/multi-color-link';
import { Injectable } from '@angular/core';
import * as go from 'gojs';
const $ = go.GraphObject.make

@Injectable({
  providedIn: 'root'
})
export class DiagramComposeService {

  constructor() { }

  defineComponentNodeWithIcon(diagram: go.Diagram, typename: string, icon: string, background: string, inports: any[], outports: any[]) {
    const node = $(go.Node, "Spot",
      $(go.Panel, "Auto",

        { minSize: new go.Size(120, 140), isPanelMain: true },
        $(go.Shape, "Rectangle",
          {
            fill: background, stroke: null, strokeWidth: 0,
            spot1: go.Spot.TopLeft, spot2: go.Spot.BottomRight
          }),
        $(go.Panel, "Table",
          $(go.TextBlock, typename,
            {
              row: 0,
              margin: 3,
              minSize: new go.Size(80, NaN),
              stroke: "white",
              font: "bold 11pt sans-serif"
            }),
          $(go.Picture, icon,
            { row: 1, width: 16, height: 16, scale: 3.0 }),
          $(go.TextBlock,
            {
              row: 2,
              margin: 3,
              editable: true,
              minSize: new go.Size(80, 40),
              stroke: "white",
              font: "bold 9pt sans-serif"
            },
            new go.Binding("text", "name").makeTwoWay())
        )
      ),
      $(go.Panel, "Vertical",
        {
          alignment: go.Spot.Left,
          alignmentFocus: new go.Spot(0, 0.5, 8, 0)
        },
        inports),
      $(go.Panel, "Vertical",
        {
          alignment: go.Spot.Right,
          alignmentFocus: new go.Spot(1, 0.5, -8, 0)
        },
        outports)
    );

    diagram.nodeTemplateMap.set(typename, node);
  }

  defineComponentNodeWithoutIcon(diagram: go.Diagram, typename: string, background: string, inports: any[], outports: any[]) {
    const node = $(go.Node, "Auto", {minSize: new go.Size(220, NaN)},
      $(go.Panel, "Auto",
        $(go.Shape, "Rectangle",
          {
            fill: background, stroke: "black", strokeWidth: 2,
            spot1: go.Spot.TopLeft, spot2: go.Spot.BottomRight,
            stretch: go.GraphObject.Uniform,
          }),
        $(go.Panel, "Table", {alignment: go.Spot.TopLeft},
          $(go.TextBlock, typename.toUpperCase(),
            {
              row: 0,
              column: 0,
              textAlign: "left",
              stroke: "#b2bec3",
              margin: new go.Margin(10,0,0,10),
              font: "bolder 20pt sans-serif"
            })
        )
      ),
      $(go.Panel, "Vertical",
        {
          alignment: go.Spot.Left,
          alignmentFocus: new go.Spot(0, 0.5, 8, 0),
          //minSize: new go.Size(120,NaN)
          stretch: go.GraphObject.Horizontal,
          margin: new go.Margin(50,0,50,0)
        },
        inports),
      $(go.Panel, "Vertical",
        {
          alignment: go.Spot.Right,
          alignmentFocus: new go.Spot(1, 0.5, -8, 0),
          //minSize: new go.Size(120,NaN)
          stretch: go.GraphObject.Horizontal,
          margin: new go.Margin(50,0,50,0)
        },
        outports)
    );

    diagram.nodeTemplateMap.set(typename, node);
  }

  definePort(name: string, leftside: boolean) {
    var port = $(go.Shape, "Rectangle",
      {
        fill: "black", stroke: null,
        desiredSize: new go.Size(8, 4),
        portId: name,  // declare this object to be a "port"
        //toMaxLinks: 1,  // don't allow more than one link into a port
        cursor: "pointer"  // show a different cursor to indicate potential link point
      });

    var lab = $(go.TextBlock, name,  // the name of the port
      { font: "12pt sans-serif", stretch: go.GraphObject.Horizontal });

    var panel = $(go.Panel, "Horizontal",
      { margin: new go.Margin(2, 0) });

    // set up the port/panel based on which side of the node it will be on
    if (leftside) {
      port.toSpot = go.Spot.Left;
      port.toLinkable = true;
      port.fromLinkable = true
      port.toLinkableSelfNode = true;
      lab.margin = new go.Margin(1, 0, 0, 5);
      panel.alignment = go.Spot.TopLeft;
      panel.add(port);
      panel.add(lab);
    } else {
      port.fromSpot = go.Spot.Right;
      port.fromLinkable = true;
      port.toLinkable = true
      port.fromLinkableSelfNode = true;
      lab.margin = new go.Margin(1, 5, 0, 0);
      panel.alignment = go.Spot.TopRight;
      panel.add(lab);
      panel.add(port);
    }
    return panel;
  }

  defineLink(diagram: go.Diagram) {
    diagram.linkTemplate = $(MultiColorLink,
      {
        //routing: go.Link.Orthogonal,
        routing: go.Link.AvoidsNodes,
        corner: 5,
        relinkableFrom: true, relinkableTo: true,
        curve: go.Link.JumpGap,
        fromEndSegmentLength: 10,
        toEndSegmentLength: 10,
      },
      //$(go.Shape, { stroke: "black", strokeWidth: 2 }),
      //$(go.Shape, { stroke: "black", fill: "gray", toArrow: "Standard" })
      this.linkPath(),
      this.linkPath()
    );
  }

  linkPath() {  // common styling for each colored section, each rendered by a different Shape
    return $(go.Shape, { isPanelMain: true, strokeWidth: 3 });
  }
}
