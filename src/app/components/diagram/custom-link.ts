import * as go from 'gojs';

// This custom-routing Link class tries to separate parallel links from each other.
  // This assumes that ports are lined up in a row/column on a side of the node.
  export class CustomLink extends go.Link {
    findSidePortIndexAndCount(node:any, port:any) {
      const nodedata = node.data;
      if (nodedata !== null) {
        const portdata = port.data;
        const side = port._side;
        const arr = nodedata[side + "Array"];
        var len = arr.length;
        for (let i = 0; i < len; i++) {
          if (arr[i] === portdata) return [i, len];
        }
      }
      return [-1, len];
    }

    override computeEndSegmentLength(node: any, port:any, spot:any, from:any) {
      const esl = super.computeEndSegmentLength(node, port, spot, from);
      const other = this.getOtherPort(port);
      if (port !== null && other !== null) {
        const thispt = port.getDocumentPoint(this.computeSpot(from));
        const otherpt = other.getDocumentPoint(this.computeSpot(!from));
        if (Math.abs(thispt.x - otherpt.x) > 20 || Math.abs(thispt.y - otherpt.y) > 20) {
          const info = this.findSidePortIndexAndCount(node, port);
          const idx = info[0];
          const count = info[1];
          if (port._side == "top" || port._side == "bottom") {
            if (otherpt.x < thispt.x) {
              return esl + 4 + idx * 8;
            } else {
              return esl + (count - idx - 1) * 8;
            }
          } else {  // left or right
            if (otherpt.y < thispt.y) {
              return esl + 4 + idx * 8;
            } else {
              return esl + (count - idx - 1) * 8;
            }
          }
        }
      }
      return esl;
    }

    override hasCurviness() {
      if (isNaN(this.curviness)) return true;
      return super.hasCurviness();
    }

    override computeCurviness() {
      if (isNaN(this.curviness)) {
        const fromnode = this.fromNode;
        const fromport = this.fromPort;
        const fromspot = this.computeSpot(true);
        const frompt = fromport!.getDocumentPoint(fromspot);
        const tonode = this.toNode;
        const toport = this.toPort;
        const tospot = this.computeSpot(false);
        const topt = toport!.getDocumentPoint(tospot);
        if (Math.abs(frompt.x - topt.x) > 20 || Math.abs(frompt.y - topt.y) > 20) {
          if ((fromspot.equals(go.Spot.Left) || fromspot.equals(go.Spot.Right)) &&
            (tospot.equals(go.Spot.Left) || tospot.equals(go.Spot.Right))) {
            const fromseglen = this.computeEndSegmentLength(fromnode, fromport, fromspot, true);
            const toseglen = this.computeEndSegmentLength(tonode, toport, tospot, false);
            const c = (fromseglen - toseglen) / 2;
            if (frompt.x + fromseglen >= topt.x - toseglen) {
              if (frompt.y < topt.y) return c;
              if (frompt.y > topt.y) return -c;
            }
          } else if ((fromspot.equals(go.Spot.Top) || fromspot.equals(go.Spot.Bottom)) &&
            (tospot.equals(go.Spot.Top) || tospot.equals(go.Spot.Bottom))) {
            const fromseglen = this.computeEndSegmentLength(fromnode, fromport, fromspot, true);
            const toseglen = this.computeEndSegmentLength(tonode, toport, tospot, false);
            const c = (fromseglen - toseglen) / 2;
            if (frompt.x + fromseglen >= topt.x - toseglen) {
              if (frompt.y < topt.y) return c;
              if (frompt.y > topt.y) return -c;
            }
          }
        }
      }
      return super.computeCurviness();
    }

    override makeGeometry() {
      const geo = super.makeGeometry();
      const colors = this.data.colors;
      if (Array.isArray(colors) && colors.length > 0) {
        const paths = Array<any>();  // find all path Shapes
        this.elements.each(elt => {
          if (elt.isPanelMain && elt instanceof go.Shape) {
            paths.push(elt);
          }
        });
        const numcolors = Math.min(colors.length, paths.length);
        if (numcolors > 0) {
          const seclen = geo.flattenedTotalLength / numcolors;  // length of each color section
          for (let i = 0; i < paths.length; i++) {  // go through all path Shapes
            const shape = paths[i];
            if (i < numcolors) {
              shape.visible = true;  // make sure this Shape can be seen
              shape.stroke = colors[i];  // and assign a color
              if (i > 0) {  // and a stroke dash array so that it only draws the needed fraction
                shape.strokeDashArray = [0, i * seclen, seclen, 99999];
              }
            } else {  // unneeded Shapes are not visible
              shape.visible = false;
            }
          }
        }
      }
      return geo;
    }
  }
  // end CustomLink class
