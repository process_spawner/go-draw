import { FakeService } from './../../services/fake.service';
import { Component, OnInit } from '@angular/core';
import * as go from 'gojs';
import { CustomLink } from './custom-link';

const $ = go.GraphObject.make

@Component({
  selector: 'app-diagram',
  templateUrl: './diagram.component.html',
  styleUrls: ['./diagram.component.css']
})
export class DiagramComponent implements OnInit {

  public diagram: go.Diagram | null = null;

  constructor(public fakeService: FakeService) { }

  ngOnInit(): void {
  }

  public ngAfterViewInit(): void {

    this.setupLayeredDigraphDiagram();
    //this.setupGridDiagram();

    this.setNodeTemplate();
    this.setupLinkTemplate();

    this.presentDiagramContent();
  }

  private setupGridDiagram() {
    this.diagram =
    $(go.Diagram, "diagramScene",  // must be the ID or reference to div
      {
        contentAlignment: go.Spot.Center,
        layout: $(go.GridLayout,{ spacing: new go.Size(150, 200), wrappingColumn: 10, isViewportSized: false }),
        scrollMode: go.Diagram.InfiniteScroll,
        allowZoom: true
      });
  }

  private setupLayeredDigraphDiagram() {
    this.diagram =
    $(go.Diagram, "diagramScene",
      {
        initialContentAlignment: go.Spot.Center,
        initialAutoScale: go.Diagram.UniformToFill,
        layout: $(go.LayeredDigraphLayout,
          {
              direction: 270,
              //isOngoing: false,
              layerSpacing: 50,
              setsPortSpots: false,
              columnSpacing: 100,
              //isRouting: true,
              //isValidLayout: true,
              //isViewportSized: true,
              //aggressiveOption: go.LayeredDigraphLayout.AggressiveMore,
              //cycleRemoveOption: go.LayeredDigraphLayout.CycleDepthFirst,
              //initializeOption: go.LayeredDigraphLayout.InitDepthFirstOut,
              layeringOption: go.LayeredDigraphLayout.LayerLongestPathSink,
              //packOption: go.LayeredDigraphLayout.PackAll
          })
      });
  }

  private presentDiagramContent() {
    this.diagram!.model = new go.GraphLinksModel(
      {
        copiesArrays: true,
        copiesArrayObjects: true,
        linkFromPortIdProperty: "fromPort",
        linkToPortIdProperty: "toPort",
        nodeDataArray:this.fakeService.getModels(),
        linkDataArray: this.fakeService.getLinks()
      });
  }


  private setNodeTemplate() {
    const portSize = new go.Size(8, 8);
    // the node template
      // includes a panel on each side with an itemArray of panels containing ports
      this.diagram!.nodeTemplate =
        $(go.Node, "Table",
          {
            locationObjectName: "BODY",
            locationSpot: go.Spot.Center,
            selectionObjectName: "BODY",
            doubleClick: function(e:any, obj:any) {
              alert('click on node');
            },
            mouseEnter: (e: any, obj: any) => {
              console.log('node mouse enter');
            },
            mouseLeave: (e: any, obj: any) => {
              console.log('node mouse leave');
            }
          },
          // the body
          $(go.Panel, "Auto",
            {
              row: 1, column: 1, name: "BODY",
              stretch: go.GraphObject.Horizontal,
            },
            $(go.Shape, "Rectangle",
              {
                fill: "#7f8fa6", stroke: null, strokeWidth: 0,
                minSize: new go.Size(60, 60),
                stretch: go.GraphObject.Horizontal,
              }),
            $(go.TextBlock,
              { margin: 10, textAlign: "center", font: "bold 14px Segoe UI,sans-serif", stroke: "#f5f6fa", editable: false, isMultiline: true, wrap: go.TextBlock.WrapFit },
              new go.Binding("text", "name").makeTwoWay())
          ),  // end Auto Panel body

          // the Panel holding the left port elements, which are themselves Panels,
          // created for each item in the itemArray, bound to data.leftArray
          $(go.Panel, "Vertical",
            new go.Binding("itemArray", "leftArray"),
            {
              row: 1, column: 0,
              itemTemplate:
                $(go.Panel,
                  {
                    _side: "left",
                    fromSpot: go.Spot.Left, toSpot: go.Spot.Left,
                    fromLinkable: true, toLinkable: true, cursor: "pointer",
                    mouseEnter: (e: any, obj: any) => console.log(obj.data)
                  },
                  new go.Binding("portId", "portId"),
                  $(go.Shape, "Circle",
                    {
                      stroke: null, strokeWidth: 0,
                      desiredSize: portSize,
                      margin: new go.Margin(5, 0)
                    },
                    new go.Binding("fill", "portColor")),


                )  // end itemTemplate
            }
          ),  // end Vertical Panel

          // the Panel holding the top port elements, which are themselves Panels,
          // created for each item in the itemArray, bound to data.topArray
          $(go.Panel, "Horizontal",
            new go.Binding("itemArray", "topArray"),
            {
              row: 0, column: 1,
              itemTemplate:
                $(go.Panel,
                  {
                    _side: "top",
                    fromSpot: go.Spot.Top, toSpot: go.Spot.Top,
                    fromLinkable: true, toLinkable: true, cursor: "pointer",
                    mouseEnter: (e: any, obj: any) => console.log(obj.data)
                  },
                  new go.Binding("portId", "portId"),
                  $(go.Shape, "Circle",
                    {
                      stroke: null, strokeWidth: 0,
                      desiredSize: portSize,
                      margin: new go.Margin(0, 5)
                    },
                    new go.Binding("fill", "portColor"))
                )  // end itemTemplate
            }
          ),  // end Horizontal Panel

          // the Panel holding the right port elements, which are themselves Panels,
          // created for each item in the itemArray, bound to data.rightArray
          $(go.Panel, "Vertical",
            new go.Binding("itemArray", "rightArray"),
            {
              row: 1, column: 2,
              itemTemplate:
                $(go.Panel,
                  {
                    _side: "right",
                    fromSpot: go.Spot.Right, toSpot: go.Spot.Right,
                    fromLinkable: true, toLinkable: true, cursor: "pointer",
                    mouseEnter: (e: any, obj: any) => console.log(obj.data)
                  },
                  new go.Binding("portId", "portId"),
                  $(go.Shape, "Circle",
                    {
                      stroke: null, strokeWidth: 0,
                      desiredSize: portSize,
                      margin: new go.Margin(5, 0)
                    },
                    new go.Binding("fill", "portColor"))
                )  // end itemTemplate
            }
          ),  // end Vertical Panel

          // the Panel holding the bottom port elements, which are themselves Panels,
          // created for each item in the itemArray, bound to data.bottomArray
          $(go.Panel, "Horizontal",
            new go.Binding("itemArray", "bottomArray"),
            {
              row: 2, column: 1,
              itemTemplate:
                $(go.Panel,
                  {
                    _side: "bottom",
                    fromSpot: go.Spot.Bottom, toSpot: go.Spot.Bottom,
                    fromLinkable: true, toLinkable: true, cursor: "pointer",
                    mouseEnter: (e: any, obj: any) => console.log(obj.data)
                  },
                  new go.Binding("portId", "portId"),
                  $(go.Shape, "Circle",
                    {
                      stroke: null, strokeWidth: 0,
                      desiredSize: portSize,
                      margin: new go.Margin(0, 5)
                    },
                    new go.Binding("fill", "portColor"))
                )  // end itemTemplate
            }
          )  // end Horizontal Panel
        );  // end Node
  }

  private setupLinkTemplate() {
    this.diagram!.linkTemplate =
    $(CustomLink,
      {
        routing: go.Link.AvoidsNodes,
        //corner: 6,
        curve: go.Link.JumpGap,
        reshapable: false,
        resegmentable: false,
        relinkableFrom: false,
        relinkableTo: false,
        fromEndSegmentLength: 30,
        toEndSegmentLength: 30,
        doubleClick: function(e: any, obj: any) {
          alert('clicked on the link');
        },
        mouseEnter: (e: any, obj: any) => {
          console.log('link mouse enter');
        },
        mouseLeave: (e: any, obj: any) => {
          console.log('link mouse leave');
        }
      },
      new go.Binding("points").makeTwoWay(),
      $(go.Shape, { isPanelMain: true, strokeWidth: 4 }),
      $(go.Shape, { isPanelMain: true, strokeWidth: 4 }),
      $(go.TextBlock, "from",
      { segmentIndex: 0, segmentOffset: new go.Point(NaN, NaN), background: "white", font: "12px Segoe UI,sans-serif",
        segmentOrientation: go.Link.Horizontal }, new go.Binding("text", "from")),
      $(go.TextBlock, "to",
      { segmentIndex: -1, segmentOffset: new go.Point(NaN, NaN), background: "white", font: "12px Segoe UI,sans-serif",
        segmentOrientation: go.Link.Horizontal }, new go.Binding("text", "to"))

    );
  }
}
