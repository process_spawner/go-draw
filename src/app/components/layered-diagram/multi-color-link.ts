import * as go from 'gojs';
// Assume that a Link may have a  linkdata.colors  property that is an Array of CSS color strings.
  // Also assume that each Link has as many Shapes in it that are marked with  isPanelMain: true
  // as you want to get a color from the linkdata.colors Array, because each Shape can only show one color.
  export class MultiColorLink extends go.Link {
    override makeGeometry() {
      const geo = super.makeGeometry();
      const colors = this.data.colors;
      if (Array.isArray(colors) && colors.length > 0) {
        const paths = Array<any>();  // find all path Shapes
        this.elements.each(elt => {
          if (elt.isPanelMain && elt instanceof go.Shape) {
            paths.push(elt);
          }
        });
        const numcolors = Math.min(colors.length, paths.length);
        if (numcolors > 0) {
          const seclen = geo.flattenedTotalLength / numcolors;  // length of each color section
          for (let i = 0; i < paths.length; i++) {  // go through all path Shapes
            const shape = paths[i];
            if (i < numcolors) {
              shape.visible = true;  // make sure this Shape can be seen
              shape.stroke = colors[i];  // and assign a color
              if (i > 0) {  // and a stroke dash array so that it only draws the needed fraction
                shape.strokeDashArray = [0, i * seclen, seclen, 99999];
              }
            } else {  // unneeded Shapes are not visible
              shape.visible = false;
            }
          }
        }
      }
      return geo;
    }
  }
  // end of MultiColorLink class
