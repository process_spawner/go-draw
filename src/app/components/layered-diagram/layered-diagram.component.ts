import { FakeComponentsService } from './../../services/fake-components.service';
import { DiagramComposeService } from './../../services/diagram-compose.service';
import { DiagramComponent } from './../diagram/diagram.component';
import { Component, OnInit } from '@angular/core';
import * as go from 'gojs';

const $ = go.GraphObject.make

@Component({
  selector: 'app-layered-diagram',
  templateUrl: './layered-diagram.component.html',
  styleUrls: ['./layered-diagram.component.css']
})

export class LayeredDiagramComponent implements OnInit {
  public diagram: go.Diagram | null = null;

  constructor(public diagramComposeService: DiagramComposeService, public fakeComponentsService: FakeComponentsService) { }

  ngOnInit(): void {
  }

  public ngAfterViewInit(): void {

    this.setupLayeredDigraphDiagram();
    this.presentDiagram()
  }

  private setupLayeredDigraphDiagram() {
    this.diagram = $(go.Diagram, "diagramScene",
      {
        initialContentAlignment: go.Spot.Center,
        initialAutoScale: go.Diagram.Uniform,
        padding: 50,
        layout: $(go.LayeredDigraphLayout,
          { direction: 0,
            layerSpacing: 50,
            columnSpacing: 100,
          }),
      }
    );
  }

  private presentDiagram() {


    this.diagramComposeService.defineComponentNodeWithoutIcon(this.diagram!, "Table", "white",
    [],
    [this.diagramComposeService.definePort("OUTISH-1", false)]);

    this.diagramComposeService.defineComponentNodeWithoutIcon(this.diagram!, "Join",  "white",
    [this.diagramComposeService.definePort("L", true), this.diagramComposeService.definePort("R", true)],
    [this.diagramComposeService.definePort("UL", false), this.diagramComposeService.definePort("ML", false), this.diagramComposeService.definePort("M", false), this.diagramComposeService.definePort("MR", false), this.diagramComposeService.definePort("UR", false)]);

    this.diagramComposeService.defineComponentNodeWithoutIcon(this.diagram!, "Project",  "white",
    [this.diagramComposeService.definePort("", true)],
    [this.diagramComposeService.definePort("OUTISH-1", false)]);

    this.diagramComposeService.defineComponentNodeWithoutIcon(this.diagram!, "Filter",  "white",
    [this.diagramComposeService.definePort("", true)],
    [this.diagramComposeService.definePort("OUTISH-1", false), this.diagramComposeService.definePort("INV", false)]);

    this.diagramComposeService.defineComponentNodeWithoutIcon(this.diagram!, "Group",  "white",
    [this.diagramComposeService.definePort("", true)],
    [this.diagramComposeService.definePort("OUTISH-1", false)]);

    this.diagramComposeService.defineComponentNodeWithoutIcon(this.diagram!, "Sort",  "white",
    [this.diagramComposeService.definePort("", true)],
    [this.diagramComposeService.definePort("OUTISH-1", false),
    this.diagramComposeService.definePort("OUTISH-2", false),
    this.diagramComposeService.definePort("OUTISH-3", false),
    this.diagramComposeService.definePort("OUTISH-4", false),
    this.diagramComposeService.definePort("OUTISH-5", false),
    this.diagramComposeService.definePort("OUTISH-6", false),
    this.diagramComposeService.definePort("OUTISH-7", false),
    this.diagramComposeService.definePort("OUTISH-8", false),
    this.diagramComposeService.definePort("OUTISH-9", false),
    this.diagramComposeService.definePort("OUTISH-10", false),
    this.diagramComposeService.definePort("OUTISH-11", false),
    this.diagramComposeService.definePort("OUTISH-12", false),
    this.diagramComposeService.definePort("OUTISH-13", false),
    this.diagramComposeService.definePort("OUTISH-14", false)]);

    this.diagramComposeService.defineComponentNodeWithoutIcon(this.diagram!, "Export",  "white",
    [this.diagramComposeService.definePort("", true)],
    []);

    this.diagramComposeService.defineLink(this.diagram!)

    this.diagram!.model = new go.GraphLinksModel(
      {
        nodeCategoryProperty: "type",
        linkFromPortIdProperty: "frompid",
        linkToPortIdProperty: "topid",
        nodeDataArray:this.fakeComponentsService.getModels(),
        linkDataArray: this.fakeComponentsService.getLinks()
      });

    this.diagram!.zoomToFit()
  }

}
